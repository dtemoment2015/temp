export function errorNotify(error, notify) {

  console.log(error);

  let data = error.response? error.response.data : {};


  let error_msg = (data.errors) ? data.errors : (data.data ? data.data : null) ;
 

  if (error_msg && error.response.status == 422) {
    let k = 0;
    Object.keys(error_msg).forEach((item) => {
      let err = error_msg[item];
      k++;
      for (let i = 0; i < err.length; i++) {
        setTimeout(() => {
          notify({
            title: 'Ошибка',
            message: err[i],
            icon: "ti-alert",
            horizontalAlign: 'right',
            verticalAlign: 'top',
            type: 'warning'
          });
        }, 100 * k);
      }
    });
  }
  if (data.message) {
    notify({
      title: 'Ошибка',
      message: data.message,
      icon: "ti-alert",
      horizontalAlign: 'right',
      verticalAlign: 'top',
      type: 'warning'
    });
  }
}

export function cardContent(model = '', id = '', params = {}) {
  this.$loading(true);
  return this.$http
    .get(`${model}${id ? `/${id}` : ''}`, {
      params: {
        ...params
      },
    })
    .then((response) => {
      return multiKeyInit(response.data.data, this);
    })
    .catch((error) => {
      errorNotify(error, this.$notify);
    })
    .finally(() => {
      setTimeout(() => {
        this.$loading(false);
      }, 400);
    });
}

export function cardUpdate(model = '', item = {}, fields = {}, method = 'PUT', child = false) {

  this.$loading(true);

  let bodyFormData = new FormData();

  bodyFormData.append('_method', method);
  let val = null;
  fields.forEach((i) => {
    if (!i.isMultiLang && item.hasOwnProperty(i.key)) {
      let key = i.key;
      if (item[key] || item[key] == 0 || item[key] === null) {
        let value = item[key];
        if (Array.isArray(value)) {
          for (let j = 0; j < value.length; j++) {
            if (key === 'new_images' && value[j].file && typeof value[j].file.name === 'string') {
              bodyFormData.append(`images[${j}]`, value[j].file);
            }

            if (key === 'tags') {
              bodyFormData.append(`${key}[${j}]`, value[j].name);
            }

            if (key === 'purchases') {
              bodyFormData.append(`${key}[${value[j].id}]`, value[j].input_data);
            }

            else if (value[j] && value[j].id) {
              bodyFormData.append(`${key}[${j}]`, value[j].id);
            }

          }
          key = null;
        }
        else if (i.type === 'select' || i.type === 'selects') {
          val = value && value['id'] ? value['id'] : null;
          key = `${key}_id`;
        }
        else {
          val = value;
        }
      }

      if (val === true) {
        val = 1;
      }
      else if (val === false) {
        val = 0;
      }

      if (key!==null) {
            bodyFormData.append(key, val);
      }
    }
  });


  let route = child === true ? model : `${model}${item.id ? `/${item.id}` : ''} `;
  return this.$http
    ({
      url: route,
      // method: 'method',
      method: 'POST',
      data: bodyFormData,
      headers: {
        "Content-Type": "multipart/form-data",
        "Accept": "application/json",
        "api-version": "1",
        "platform": "web"
      }
    })
    .then((response) => {

      this.$store.dispatch("setLastUpdate", new Date());

      this.$notify({
        title: "Данные",
        message: "Данные успешно применились",
        icon: "ti-save",
        horizontalAlign: "right",
        verticalAlign: "top",
        type: "success",
      });

      return response.data.data;

    })
    .catch((error) => {
      errorNotify(error, this.$notify);
      return null;
    })
    .finally(() => {
      setTimeout(() => {
        this.$loading(false);
      }, 400);
    });
}

export function multiKeysInit(items = [], app) {

  if (app.fields && app.fields.length) {
    if (items.length) {
      for (let i = 0; i < items.length; i++) {
        items[i] = multiKeyInit(items[i], app, i);
      }
    } else {
      multiKeyInit({}, app, 0);
    }
  }
  return items;
}

export function multiKeyInit(item = {}, app, count = 0) {

  if (app.fields && app.fields.length) {
    Object.keys(app.$store.state.languages).forEach((lang) => {

      let translationObject = {};
      if (item.translations) {
        translationObject = item.translations.find((obj) => {
          return obj.locale === lang;
        });
      }
      Object.keys(app.fields).forEach((key) => {
        let type = null;

        if ((app.fields[key].type === 'field' || app.fields[key].type === 'textarea')) {
          type = '';
        }
        else if (app.fields[key].type === 'checkbox') {
          type = false;
        }
        else if (app.fields[key].type === 'selects') {
          type = [];
        }
        else if (app.fields[key].type === 'tag') {
          type = [];
        }
        else if (app.fields[key].type === 'select') {
          type = null;
        }
        else if (app.fields[key].type === 'html') {
          type = '';
        }
        else if (app.fields[key].type === 'object') {
          if (app.fields[key].subFields && app.fields[key].subFields.length) {
            type = {};
            for (let i = 0; i < app.fields[key].subFields.length; i++) {
              type[app.fields[key].subFields[i].key] = null
            }
          }
        }
        if (app.fields[key].isMultiLang === true) {

          if (count === 0) {
            app.fields.push({
              key: app.fields[key].key + ":" + lang + "",
              label: app.fields[key].label + " (" + lang + ")",
              type: app.fields[key].type,
              textType: app.fields[key].textType,
              langKey: lang,
            });
          }
          item[app.fields[key].key + ":" + lang + ""] =
            translationObject && translationObject[app.fields[key].key]
              ? translationObject[app.fields[key].key]
              : type;

          delete item[app.fields[key].key];
          // delete app.fields[key];
        }
        else if (!item[app.fields[key].key]) {
          item[app.fields[key].key] = type;
        }
      });

    });
  }
  return item;
}

export function moveIndex(arr, old_index, new_index) {
  while (old_index < 0) {
    old_index += arr.length;
  }
  while (new_index < 0) {
    new_index += arr.length;
  }
  if (new_index >= arr.length) {
    var k = new_index - arr.length;
    while ((k--) + 1) {
      arr.push(undefined);
    }
  }
  arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);

  return arr;
}

export function pushLang() {
  let languages = this.$store.state.languages;
  let langs = [];
  for (let i = 0; i < Object.keys(languages).length; i++) {
    langs.push({
      id: Object.keys(languages)[i], title: languages[Object.keys(languages)[i]]
    });
  }
  this.fields.unshift(
    {
      label: "Язык всего элемента",
      key: "language_code",
      type: "static_radio",
      items: langs,
    },
  );
}