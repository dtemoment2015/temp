import Vue from 'vue'

export function maxChar(data, set) {
    if (data)
        return data.length > set ? data.substring(0, set) + '...' : data;
    return "";
}

export function  dateFormat(val){
    if (val && typeof val == 'string') {
     let date = new Date(val);
         return date.toLocaleDateString();
    }

    return 'Дата не установлена';
 }
export function  dateTimeFormat(val){
    if (val && typeof val == 'string') {
     let date = new Date(val);
         return date.toLocaleString();
    }

    return 'Дата не установлена';
 }

export function month(data) {
    data = data.toString();

    switch (data) {
        case '1': {
            return 'Январь';
            break;
        }
        case '2': {
            return 'Февраль';
            break;
        }
        case '3': {
            return 'Март';
            break;

        }
        case '4': {
            return 'Апрель';
            break;
        }
        case '5': {
            return 'Май';
            break;
        }
        case '6': {
            return 'Июнь';
            break;
        }
        case '7': {
            return 'Июнь';
            break;
        }
        case '8': {
            return 'Август';
            break;
        }
        case '9': {
            return 'Сентябрь';
            break;
        }
        case '10': {
            return 'Октябрь';
            break;
        }
        case '11': {
            return 'Ноябрь';
            break;
        }
        case '12': {
            return 'Декабрь';
            break;
        }
        default: {
            return 'Не найден';
            break;
        }

    }
}

export function spaceBetweenNumber(data) {
    if (data) return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");

    return "0";
}

export function fancyTimeFormat (seconds) {

    if (seconds <= 0) {
        return 0
    }

    const format = val => `0${Math.floor(val)}`.slice(-2)
    // const hours = seconds / 3600
    const minutes = (seconds % 3600) / 60
  
    return [minutes, seconds % 60].map(format).join(':')

  }


export function dateTime(mysql_string) {

    var t = null;

    if (typeof mysql_string === 'string') {
        t = mysql_string.split(/[- :]/);
    }

    return (t[2] || 0) + '.' + (t[1] || 0) + '.' + (t[0] || 0) + ' ' + (t[3] || 0) + ':' + (t[4] || 0);
}

const filters = { maxChar, dateTime, spaceBetweenNumber, month, fancyTimeFormat, dateFormat, dateTimeFormat };

export default filters

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})
