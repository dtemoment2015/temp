// https://vuex.vuejs.org/en/actions.html

export default {
    setChecked({ commit }, payload) {
        commit('SET_CHECKED', payload);
    },
    setUser({ commit }, payload) {
        commit('SET_USER', payload);
    },
    setNewOrder({ commit }, payload) {
        commit('SET_newOrder', payload);
    },
    openModal({ commit }, payload) {
        commit('OPEN_MODAL', payload);
    },
    selectValue({ commit }, payload) {
        commit('SET_VALUE', payload);
    },
    setLastUpdate({ commit }, payload) {
        commit('SET_LAST_UPDATE', payload);
    },
    setLanguages({ commit }, payload) {
        commit('SET_LANGUAGES', payload);
    },
    setStats({ commit }, payload) {
        commit('SET_STATS', payload);
    },
    setLanguage({ commit }, payload) {
        commit('SET_LANGUAGE', payload);
    },
    setLocations({ commit }, payload) {
        commit('SET_LOCATION', payload);
    },
    setMessage({ commit }, payload) {
        commit('SET_MESSAGE', payload);
    },
    setRoles({ commit }, payload) {
        commit('SET_ROLE', payload);
    },
    setRoutes({ commit }, payload) {
        commit('SET_ROUTES', payload);
    },
    setBusiness({ commit }, payload) {
        commit('SET_BUSINESS', payload);
    },
    setSchedule({ commit }, payload) {
        commit('SET_SCHEDULE', payload);
    }
}
