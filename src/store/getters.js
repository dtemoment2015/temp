// https://vuex.vuejs.org/en/getters.html

export default {
    CHECKED: state => {
        return state.checked;
    }
}