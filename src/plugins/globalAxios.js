import Vue from 'vue';
import axios from 'axios';
Vue.prototype.$http = axios;

if (typeof Storage !== "undefined") {
  let UserToken = localStorage.getItem('token');
  if (UserToken) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] = UserToken;
  }
}

Vue.prototype.$http.defaults.headers.common['platform'] = 'web';
Vue.prototype.$http.defaults.headers.common['Accept'] = 'application/json';
//Vue.prototype.$http.defaults.baseURL = 'http://127.0.0.1:8001/api/v1/admin/'; //DEV

Vue.prototype.$http.defaults.baseURL = 'https://api.1vine.uz/api/v1/admin/';  //PROD

Vue.prototype.$http.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {

    return Promise.reject(error);
  }
);

Vue.prototype.$http.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {


    if (error && error.response) {

      if (error.response.status == '401') {

      }
      else if (error.response.status == '422') {

      }
      else if (error.response.status == '500') {
        alert('Ошибка сервера.')
      }
      else if (error.response.status == '429') {
        alert('Данный IP адрес заблокирован на 1 минуту.')
      }
      else if (error.response.status == '413') {

      }
    }

    else if (!error.response) {
      alert('Запрет на уровне NGINX - ограничение размера файла, а также данных  на сервер (CORS ERROR)')
    }

    return Promise.reject(error);
  }
);

export default Vue.prototype.$http;
