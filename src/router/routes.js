import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
import NotFound from "@/pages/NotFoundPage.vue";

import store from '@/store';




let server = [
    {
        path: "dashboard",
        name: "Главная",
        component: () => import('@/pages/Dashboard.vue')
    },
    {
        path: "warehouse",
        name: "Склад",
        component: () => import('@/pages/Warehouse.vue')
    },
    {
        path: "clients",
        name: "Покупатели",
        component: () => import('@/pages/Clients.vue')
    },
    {
        path: "imports",
        name: "Импорты",
        component: () => import('@/pages/Imports.vue')
    },
    {
        path: "tp_stats",
        name: "Статистика ТП",
        component: () => import('@/pages/TPStats.vue')
    },
    {
        path: "quit",
        name: "Выход",
        component: () => import('@/pages/Quit.vue')
    },
    {
        path: "dealers",
        name: "Бизнесы",
        component: () => import('@/pages/Dealers.vue')
    },
    {
        path: "shops",
        name: "Магазины",
        component: () => import('@/pages/Shops.vue')
    },

    {
        path: "roles",
        name: "Роли",
        component: () => import('@/pages/Roles.vue')
    },
    {
        path: "transactions",
        name: "Транзакции",
        component: () => import('@/pages/Transactions.vue')
    },
    {
        path: "posts",
        name: "Новости",
        component: () => import('@/pages/Posts.vue')
    },
    {
        path: "orders",
        name: "Заказы",
        component: () => import('@/pages/Orders.vue')
    },
    {
        path: "texts",
        name: "Системные слова",
        component: () => import('@/pages/Texts.vue')
    },
    {
        path: "users",
        name: "Администраторы",
        component: () => import('@/pages/Users.vue')
    },
    {
        path: "navigations",
        name: "Экраны",
        component: () => import('@/pages/Navigations.vue')
    },
    {
        path: "my-orders",
        name: "Мои заказы",
        component: () => import('@/pages/MyOrders.vue')
    },
    {
        path: "my-shops",
        name: "Мои магазины",
        component: () => import('@/pages/MyShops.vue')
    },
    {
        path: "supervisor_orders",
        name: "Заказы",
        component: () => import('@/pages/SupervisorOrders.vue')
    },
    {
        path: "supervisor_orders",
        name: "Пользователи",
        component: () => import('@/pages/SupervisorOrders.vue')
    },
    {
        path: "supervisor_users",
        name: "Пользователи",
        component: () => import('@/pages/SupervisorUsers.vue')
    },
    {
        path: "supervisor_shops",
        name: "Пользователи",
        component: () => import('@/pages/SupervisorShops.vue')
    },
    {
        path: "my-dealers",
        name: "Мои бизнесы",
        component: () => import('@/pages/MyDealers.vue')
    },
];

const routes = [
    {
        path: "/",
        component: DashboardLayout,
        redirect: "/dashboard",
        children: server
    },
    { path: "*", component: NotFound }
];

export default routes;
