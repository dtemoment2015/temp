export default {
    languages: {
        'ru': 'Русский'
    },
    location: {},
    lastMessage: {},
    roles: {},
    stats: [],
    routes: {},
    shedules: {},
    language: 'ru',
    checked: null,
    business: null,
    user: null,
    newOrder: null,
    modals: {
        category: {
            modalCreate: "category",
            fields: [
                {
                    col: ' col-12 col-md-12',
                    key: "title",
                    label: "Наименование",
                    type: "field",
                    textType: "text",
                    isMultiLang: true
                },
                {

                    col: ' col-12 col-md-12',
                    key: "is_active",
                    label: "Активен",
                    type: "checkbox",
                },
                //   {
                //   key: "position",
                //   label: "Позиция",
                //   type: "field",
                //   textType: "number",
                // },
                {
                    col: ' col-12',
                    key: "parent",
                    label: "Главная категория",
                    type: "select",
                    modal: 'category',
                    selectKey: "title",
                    textType: "text",
                    disableLink: true,
                    prefix: 'category_category_modal'
                },

            ],
            isAdd: true,
            openItem: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_category",
            itemLink: "warehouse/categories",
            setLink: "warehouse/categories",
            columns: [
                {
                    key: "title",
                    title: "Наименование",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "parent",
                    keyAs: "category",
                    title: "Главная категория",
                    checkbox: false,
                    control: false,
                    align: "left",
                    timer: false,
                    object: 'title',
                },
            ],
            params: {
            }
        },
        product: {
            modalCreate: "product",
            fields: [
                {
                    label: "Название",
                    key: "title",
                    type: "field",
                    textType: "text",
                    isMultiLang: true
                },

                {
                    col: ' col-12 col-md-12',
                    key: "categories",
                    label: "Категории (тут оставим?)",
                    type: "selects",
                    selectKey: "title",
                    modal: "category",
                },
                {
                    col: ' col-12 col-md-12',
                    key: "brand",
                    label: "Бренд",
                    type: "select",
                    selectKey: "name",
                    modal: "brand",
                    prefix: 'product_brand'
                },
                {
                    col: ' col-12 col-md-12',
                    key: "name",
                    label: "Класс",
                    type: "select",
                    selectKey: "title",
                    modal: "name",
                    prefix: 'product_class'
                },
                {
                    key: "vendor_code",
                    label: "Артикул (группа)",
                    type: "field",
                    textType: "text",
                },
            ],
            openItem: false,
            isAdd: true,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_product",
            itemLink: "warehouse/products",
            setLink: "warehouse/products",
            columns: [
                {
                    key: "display_name",
                    title: "Наименование",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                }
            ],
            params: {
            }
        },
        option: {
            modalCreate: "option",
            fields: [
            ],
            isAdd: false,
            openItem: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_option",
            itemLink: "warehouse/options",
            setLink: "",
            columns: [
                {
                    key: "display_name",
                    title: "Наименование",
                    checkbox: true,
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "price",
                    title: "Цена",

                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "quantity",
                    title: "На складе",

                    control: false,
                    align: "left",
                    timer: false,
                },
            ],
            params: {
            }
        },
        option_key: {
            modalCreate: "option",
            fields: [
            ],
            isAdd: false,
            openItem: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_option",
            itemLink: "warehouse/options",
            setLink: "",
            columns: [
                {
                    key: "display_name",
                    title: "Наименование",
                    checkbox: true,
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    title: 'Введите количество',
                    key: "input_data",
                    input: 'number'
                },
                {
                    key: "price",
                    title: "Цена",
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "quantity",
                    title: "На складе",

                    control: false,
                    align: "left",
                    timer: false,
                },
            ],
            params: {
            }
        },
        // brand: {
        //     modalCreate: "brand",
        //     fields: [
        //     ],
        //     isAdd: true,
        //     isUpdate: true,
        //     searchDisable: true,
        //     removeLink: "",
        //     cardLink: "item_brand",
        //     itemLink: "warehouse/brands",
        //     setLink: "warehouse/brands",
        //     columns: [
        //         {
        //             key: "name",
        //             title: "Наименование",
        //             checkbox: true,
        //             control: false,
        //             align: "left",
        //             timer: false,
        //         }
        //     ],
        //     params: {
        //     }
        // },
        brand: {
            modalCreate: "brand",
            openItem: false,
            fields: [
                {
                    col: ' col-12 col-md-12',
                    key: "name",
                    label: "Наименование",
                    type: "field",
                    textType: "text",
                },
                {
                    key: "position",
                    label: "Позиция",
                    type: "field",
                    textType: "number",
                },
                {
                    col: ' col-12 col-md-4 ',
                    key: "image",
                    label: "Логотип",
                    type: "img",
                },
            ],
            isAdd: true,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_brand",
            itemLink: "warehouse/brands",
            setLink: "warehouse/brands",
            columns: [
                {
                    key: "name",
                    title: "Наименование",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                }
            ],
            params: {
            }
        },
        status_order: {
            modalCreate: "status",
            fields: [
            ],
            isAdd: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_status",
            itemLink: "statuses",
            setLink: "",
            columns: [
                {
                    key: "title",
                    title: "Название",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                }
            ],
            params: {
                type: 'order'
            }
        },
        status_transaction: {
            modalCreate: "status",
            fields: [
            ],
            isAdd: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_status",
            itemLink: "statuses",
            setLink: "",
            columns: [
                {
                    key: "title",
                    title: "Название",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                }
            ],
            params: {
                type: 'transaction'
            }
        },
        shop: {
            modalCreate: "shop",
            fields: [
            ],
            isAdd: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_shop",
            itemLink: "shops",
            setLink: "",
            columns: [
                {
                    key: "name",
                    title: "Название",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "address",
                    title: "Адрес",
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "dealer",
                    object: "phone",
                    control: false,
                    disableLink: true,
                    align: "left",
                    title: "Номер владельца",
                },
                {
                    key: "dealer",
                    object: "llc_name",
                    title: "Оргранизация",
                    control: false,
                    disableLink: true,
                    align: "left",
                    timer: false,
                },

            ],
            params: null
        },
        my_shop: {
            modalCreate: "shop",
            fields: [
            ],
            isAdd: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_shop",
            itemLink: "my-shops",
            setLink: "",
            columns: [
                {
                    key: "name",
                    title: "Название",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "address",
                    title: "Адрес",
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "dealer",
                    object: "phone",
                    control: false,
                    disableLink: true,
                    align: "left",
                    title: "Номер владельца",
                },
                {
                    key: "dealer",
                    object: "llc_name",
                    title: "Оргранизация",
                    control: false,
                    disableLink: true,
                    align: "left",
                    timer: false,
                },
            ],
            params: null
        },
        name: {
            modalCreate: "name",
            openItem: false,
            fields: [
                {
                    label: "Название",
                    key: "title",
                    type: "field",
                    textType: "text",
                    isMultiLang: true
                },

            ],
            isAdd: true,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_name",
            itemLink: "warehouse/names",
            setLink: "warehouse/names",
            columns: [
                {
                    key: "title",
                    title: "Наименование",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                }
            ],
            params: {
            }
        },
        serial: {
            modalCreate: "name",
            fields: [
            ],
            isAdd: false,
            isUpdate: true,
            searchDisable: true,
            removeLink: "",
            cardLink: "item_serial",
            itemLink: "warehouse/serials",
            setLink: "",
            columns: [

                {
                    key: "barcode",
                    title: "с/н",
                    control: false,
                    checkbox: true,
                    align: "left",
                    timer: false,
                },
                {
                    key: "option",
                    title: "Товар",
                    control: false,
                    align: "left",
                    object: "display_name",
                    timer: false,
                },
            ],
            params: {
            }
        },
        feature: {
            modalCreate: "name",
            fields: [
            ],
            isAdd: true,
            openItem: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_feature",
            itemLink: "warehouse/features",
            setLink: "warehouse/features",
            columns: [
                {
                    key: "title",
                    title: "Наименование",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                }
            ],
            params: {
            }
        },
        role: {
            modalCreate: "role",
            fields: [
            ],
            isAdd: false,
            openItem: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_role",
            itemLink: "roles",
            setLink: "roles",
            columns: [
                {
                    key: "title",
                    title: "Наименование",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },

            ],
            params: null
        },
        dealer: {
            modalCreate: "role",
            fields: [
            ],
            isAdd: false,
            openItem: false,
            isUpdate: true,
            searchDisable: false,
            removeLink: "",
            cardLink: "item_dealer",
            itemLink: "dealers",
            setLink: "dealers",
            columns: [
                {
                    key: "phone",
                    title: "Номер телефона",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "first_name",
                    title: "Имя",
                    checkbox: false,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "last_name",
                    title: "Фамилия",
                    checkbox: false,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "llc_name",
                    title: "Название огранизации",
                    checkbox: false,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "trademark",
                    title: "Торговая марка",
                    checkbox: false,
                    control: false,
                    align: "left",
                    timer: false,
                },


            ],
            params: null
        },
        navigation: {
            modalCreate: "navigation",
            fields: [
            ],
            isAdd: false,
            openItem: false,
            isUpdate: true,
            searchDisable: true,
            removeLink: "",
            cardLink: "item_navigation",
            itemLink: "navigations",
            setLink: "navigations",
            columns: [
                {
                    key: "title",
                    title: "Название",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "roles",
                    title: "Роли",
                    checkbox: false,
                    control: false,
                    disableLink: true,
                    object: "title",
                    timer: false,
                },
            ],
            params: null
        },
        block: {
            modalCreate: "block",
            fields: [
            ],
            isAdd: false,
            openItem: false,
            isUpdate: true,
            searchDisable: true,
            removeLink: "",
            cardLink: "item_block",
            itemLink: "blocks",
            setLink: "blocks",
            columns: [
                {
                    key: "code",
                    title: "Название",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
            ],
            params: null
        },
        user: {
            modalCreate: "user",
            fields: [
            ],
            isAdd: false,
            openItem: false,
            isUpdate: true,
            searchDisable: true,
            removeLink: "",
            cardLink: "item_user",
            itemLink: "users",
            setLink: "users",
            columns: [
                {
                    key: "name",
                    title: "Имя",
                    checkbox: true,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "phone",
                    title: "Номер телефона",
                    checkbox: false,
                    control: false,
                    align: "left",
                    timer: false,
                },
                {
                    key: "role",
                    title: "Роль",
                    object: 'title',

                },
            ],
            params: null
        },
    },
    modalName: null,
    valueSelected: null,
    lastUpdate: null,
    items: {
        item_option: 'Option',
        item_brand: 'Brand',
        item_category: 'Category',
        item_name: 'Name',
        item_feature: 'Feature',
        item_subfeature: 'Subfeature',
        item_value: 'Value',
        item_product: 'Product',
        item_serial: 'Serial',
        item_sticker: 'Sticker',
        item_user: 'User',
        item_shop: 'Shop',
        item_dealer: 'Dealer',
        item_order: 'Order',
        item_purchase: 'Purchase',
        item_transaction: 'Transaction',
        item_post: 'Post',
        item_text: 'Text',
        item_role: 'Role',
        item_user_stat: 'UserStat',
        item_navigation: 'Navigation',
        item_client: 'Client',
    }
}