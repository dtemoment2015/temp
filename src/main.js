import Vue from "vue";
import App from "./App";
import router from "./router/index";
import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";
import store from './store';
import VueNativeNotification from 'vue-native-notification';
import StarRating from 'vue-star-rating'
import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css'
import Contents from "@/components/Contents";
Vue.component('datetime', Datetime);
import VueMask from 'v-mask'
Vue.use(VueMask);
Vue.use(PaperDashboard);



//MAP
// import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
// import 'leaflet/dist/leaflet.css';
// Vue.component('l-map', LMap);
// Vue.component('l-tile-layer', LTileLayer);
// Vue.component('l-marker', LMarker);

//CARDS

import VueGeolocation from 'vue-browser-geolocation';
Vue.use(VueGeolocation);
Vue.component('Contents', Contents);
import Tabs from "@/components/Tabs";
import FieldsList from "@/components/Item/FieldsList";
import CardItem from '@/components/Item/CardItem'
Vue.component('location', () => import("@/components/Location"));
Vue.component('role', () => import("@/components/Role"));
Vue.component('sub-field-list', () => import("@/components/Item/SubFieldList"));
Vue.component('img-upload', () => import("@/components/Inputs/ImgUpload"));
Vue.component('zip-upload', () => import("@/components/Inputs/zipUpload"));
Vue.component('tags', () => import("@/components/Tags"));
Vue.component('images', () => import("@/components/Images"));
Vue.component('selectors', () => import("@/components/Inputs/Selectors"));
Vue.component('radio-list', () => import("@/components/Filters/RadioList"));
Vue.component('selector', () => import("@/components/Inputs/Selector"));
Vue.component('language-list', () => import("@/components/Filters/LanguageList"));
Vue.component('tabs', Tabs);
Vue.component('FieldsList', FieldsList);
Vue.component('card-item', CardItem);
Vue.component('filter-item', () => import('@/components/Filter'));
Vue.component('list', () => import('@/components/List'));
Vue.component('star-rating', StarRating)
let items = store.state.items;
Object.keys(items).forEach(key => {
  console.log(items[key]);
  Vue.component(key, () => import(`@/components/Item/${items[key]}/index`));
});

try {
  Notification.requestPermission()
    .then(() => {
    });
  Vue.use(VueNativeNotification, {
    requestOnNotify: true
  });
} catch (error) {

}

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
