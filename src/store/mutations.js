export default {
    SET_CHECKED: (state, payload) => {
        state.checked = payload;
    },
    SET_USER: (state, payload) => {
        state.user = payload;
    },
    SET_newOrder: (state, payload) => {
        state.newOrder = payload;
    },
    OPEN_MODAL: (state, payload) => {
        state.modalName = payload;
    },
    SET_VALUE: (state, payload) => {
        state.valueSelected = payload;
    },
    SET_LAST_UPDATE: (state, payload) => {
        state.lastUpdate = payload;
    },
    SET_LANGUAGES: (state, payload) => {
        state.languages = payload;
    },
    SET_MESSAGE: (state, payload) => {
        state.lastMessage = payload;
    },
    SET_BUSINESS: (state, payload) => {
        state.business = payload;
    },
    SET_LANGUAGE: (state, payload) => {
        state.language = payload;
    },
    SET_LOCATION: (state, payload) => {
        state.location = payload;
    },
    SET_ROLE: (state, payload) => {
        state.roles = payload;
    },
    SET_STATS: (state, payload) => {
        state.stats = payload;
    },
    SET_ROUTES: (state, payload) => {
        state.routes = payload;
    },
    SET_SCHEDULE: (state, payload) => {
        state.schedules = payload;
    }
}