import Notify from "vue-notifyjs";
import SideBar from "@/components/SidebarPlugin";
import GlobalComponents from "./globalComponents";
import GlobalDirectives from "./globalDirectives";
import {VueEditor} from "vue2-editor";
import Multiselect from 'vue-multiselect'

import "./globalAxios";
import "./filtersTemplate";
import VueLoading from 'vuejs-loading-plugin';
import vmodal from 'vue-js-modal';
import {globalCheckbox} from 'vue-material-checkbox';
import {globalRadio} from 'vue-material-radio';
import Paginate from 'vuejs-paginate';
import dialog from 'vuejs-dialog';
import "es6-promise/auto";
import "bootstrap/dist/css/bootstrap.css";
import "@/assets/sass/paper-dashboard.scss";
import "@/assets/css/themify-icons.css";
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';

import VueSocketIOExt from 'vue-socket.io-extended';
import io from 'socket.io-client';

  //const socket = io('https://api.nsumo.uz',  { });

export default {
  install(Vue) {
    // Vue.use(VueSocketIOExt, socket);
    Vue.use(globalCheckbox);
    Vue.use(dialog);
    Vue.use(GlobalComponents);
    Vue.use(GlobalDirectives);
    Vue.use(vmodal, {
      dynamic: true,
      dynamicDefaults: {
        clickToClose: false
      }
    });
   
    // Vue.use(new VueSocketIO({
    //   debug: false,
    //   connection: 'https://socket.glamourboutique.uz',
    //   options: { path: "" }
    // }));

    Vue.use(SideBar);
    Vue.use(Notify);
    Vue.component('multiselect', Multiselect)
    Vue.component('paginate', Paginate);
    Vue.component('VueEditor', VueEditor);
    Vue.component('Radio', globalRadio);
    Vue.use(VueLoading, {
      dark: true,
      text: 'Загрузка...',
      loading: false,
      background: 'rgb(255,255,255,0.6)',
    })
  }
}
